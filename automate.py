from random import *
from tkinter import *
import argparse

color_white = "#ffffff"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_red = "#c0392b"
color_green = "#6B8E23"

def genererAutomate(lignes=10, colonnes=10, proba=0.6):
    automate = []
    for row in range(lignes):
        ligne = []
        for col in range(colonnes):
            if random() < proba:
                ligne.append("arbre")
            else:
                ligne.append("vide")    
        automate.append(ligne)
    return automate

def nbCellFeuVoisine(automate, lignes, colonnes, ligne, colonne):
    compteur = 0
    if ligne > 0 and automate[ligne-1][colonne] == "feu":
        compteur+=1
    if ligne < lignes-1 and automate[ligne+1][colonne] == "feu":
        compteur+=1
    if colonne > 0 and automate[ligne][colonne-1] == "feu":
        compteur+=1
    if colonne < colonnes-1 and automate[ligne][colonne+1] == "feu":
        compteur+=1
    return compteur

def calculEtatSuivantCell(automate, lignes, colonnes, ligne, colonne):
    if automate[ligne][colonne] == "vide" or automate[ligne][colonne] == "cendre":
        return "vide"
    elif automate[ligne][colonne] == "feu" :
        return "cendre"
    elif btn_regle.cget("text") == "Regle n°1" and nbCellFeuVoisine(automate, lignes, colonnes, ligne, colonne) > 0:
        return "feu"
    elif  1-(1/(nbCellFeuVoisine(automate, lignes, colonnes, ligne, colonne)+1)) > random():
        return "feu"
    else:
        return "arbre"

def calculEtatSuivant(automate, lignes, colonnes):
    etat_suivant = []
    for row in range(lignes):
        ligne = []
        for col in range(colonnes):
            ligne.append(0)
        etat_suivant.append(ligne)

    for ligne in range(lignes):
        for colonne in range(colonnes):
            etat_suivant[ligne][colonne] = calculEtatSuivantCell(automate, lignes, colonnes, ligne, colonne)
    
    for ligne in range(lignes):
        for colonne in range(colonnes):
            automate[ligne][colonne]=etat_suivant[ligne][colonne]

#deroulement simulation
def lancementSimulation(generation):
    btn_demarrer.config(text="Generation #%d" % generation) 
    calculEtatSuivant(simulation, args.rows, args.cols)
    generation+=1
    updateSimulation(simulation, args.rows, args.cols)
    frame.after(int(args.speed*1000), lancementSimulation, generation)


#Fonction création grille canvas
def dessinerSimulation(lignes, colonnes, taille):
    #dessin des cells
    for ligne in range(lignes):
        for colonne in range(colonnes):
            cnvs_simulation.create_rectangle(colonne*taille, ligne*taille, (colonne*taille)+taille, (ligne*taille)+taille, fill=color_white, tags="%d,%d"% (ligne, colonne))

#fonction update du canvas
def updateSimulation(automate, lignes, colonnes):
    for ligne in range(lignes):
        for colonne in range(colonnes):
            if automate[ligne][colonne] == "arbre" :
                new_color = color_green
            elif automate[ligne][colonne] == "cendre" :
                new_color = color_grey
            elif automate[ligne][colonne] == "feu" :
                new_color = color_red
            else: 
                new_color = color_white
            cnvs_simulation.itemconfigure("%d,%d"%(ligne, colonne), fill=new_color)


#Fonctions callback
def callback_clickcnvs(event):
    colonne = int(event.x / args.cell_size)
    ligne = int(event.y / args.cell_size)
    if simulation[ligne][colonne] == "arbre":
        simulation[ligne][colonne] = "feu"
    elif simulation[ligne][colonne] == "feu":
        simulation[ligne][colonne] = "arbre"
    updateSimulation(simulation, args.rows, args.cols)

def callback_demarrer():
    btn_demarrer.config(state = DISABLED)
    btn_regle.config(state = DISABLED)
    lancementSimulation(0)

def callback_regle():
    if btn_regle.cget("text") == "Regle n°1":
        btn_regle.config(text="Regle n°2")
    else:
        btn_regle.config(text="Regle n°1")

    
#Main
if __name__ == '__main__':
    #Gestion Argumets/options
    parser = argparse.ArgumentParser(description="réalise la simulation d'un feu de forêt à partir du modéle d'un automate cellulaire")
    parser.add_argument("-rows", help="nombre de lignes de cellules dans la simulation", type=int, default=10)
    parser.add_argument("-cols", help="nombre de colonnes de cellules dans la simulation", type=int, default=10)
    parser.add_argument("-cell_size", help="taille des cellules dans la simulation(en pixels)", type=int, default=35)
    parser.add_argument("-afforestation", help="taux de boisement dans la simulation", type=float, default=0.6)
    parser.add_argument("-speed", help="vitesse de l'animation de la simulations en seconde", type=float, default=0.5)
    args = parser.parse_args()



    #Interface Graphique
    frame = Tk()
    frame.title("Simulation")
    frame.resizable(width=False, height=False)
    
    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=5)
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)

    btn_demarrer = Button(frame,
                             compound="left",
                             text='Demarrer',
                             command=callback_demarrer)
    btn_demarrer.grid(row=0, column=0)

    btn_regle = Button(frame,
                             compound="left",
                             text='Regle n°1',
                             command=callback_regle)
    btn_regle.grid(row=0, column=1)

    cnvs_simulation = Canvas(frame,
                                bg=color_white,
                                width=(args.cell_size*args.cols),
                                height=(args.cell_size*args.rows))
    cnvs_simulation.grid(row=1, column=0, columnspan=2)
    cnvs_simulation.bind('<Button-1>', callback_clickcnvs)

    simulation = genererAutomate(args.rows, args.cols, args.afforestation)
    dessinerSimulation(args.rows, args.cols, args.cell_size)
    updateSimulation(simulation, args.rows, args.cols)
    frame.mainloop()

