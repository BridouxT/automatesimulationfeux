import unittest
from automate import genererAutomate
from automate import calculEtatSuivant
from automate import calculEtatSuivantCell
from automate import nbCellFeuVoisine

class TestAutomateFunctions(unittest.TestCase):

    def test_genererAutomate(self):
        #proba 1 de taux de boisement
        automate = genererAutomate(100, 100, 1)
        self.assertEqual(len(automate), 100)
        for row in range(100):
            self.assertEqual(len(automate[row]), 100)
            for col in range(100):
                self.assertEqual(automate[row][col], "arbre")
        
        #proba 0 de taux de boisement
        automate = genererAutomate(100, 100, 0)
        for row in range(100):
            for col in range(100):
                self.assertEqual(automate[row][col], "vide")
                
        #Valeur par défaut(lignes=10, colonne=10, taux par défaut 0.6)
        automate = genererAutomate()
        compteurArbre = 0
        self.assertEqual(len(automate), 10)
        for row in range(10):
            self.assertEqual(len(automate[row]), 10)
            for col in range(10):
                if automate[row][col] == "arbre":
                    compteurArbre += 1
        self.assertTrue(compteurArbre > 45 & compteurArbre < 75)        

    def test_calculEtatSuivantCell(self):
        #test vide -> vide
        automate = [["vide", "vide", "vide"],["vide", "vide", "vide"],["vide", "vide", "vide"]]
        self.assertEqual("vide", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        #test feu -> cendre
        automate = [["vide", "vide", "vide"],["vide", "feu", "vide"],["vide", "vide", "vide"]]
        self.assertEqual("cendre", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        #test cendre -> vide
        automate = [["vide", "vide", "vide"],["vide", "cendre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual("vide", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        #test arbre -> arbre ou feu
        automate = [["vide", "vide", "vide"],["vide", "arbre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual("arbre", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        automate = [["vide", "feu", "vide"],["vide", "arbre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        automate = [["vide", "vide", "vide"],["feu", "arbre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        automate = [["vide", "vide", "vide"],["vide", "arbre", "vide"],["vide", "feu", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        automate = [["vide", "vide", "vide"],["vide", "arbre", "feu"],["vide", "vide", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(automate, 3, 3, 1, 1))
        automate = [["vide", "feu", "vide"],["feu", "arbre", "feu"],["vide", "feu", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(automate, 3, 3, 1, 1))

    def test_calculEtatSuivant(self):
        automate = [["vide", "arbre", "cendre"],["arbre", "feu", "arbre"],["arbre", "arbre", "vide"]]
        expected_result = [["vide", "feu", "vide"],["feu", "cendre", "feu"],["arbre", "feu", "vide"]]
        self.assertEqual(expected_result, calculEtatSuivant(automate, 3, 3))

    def test_nbCellFeuVoisine(self):
        automate = [["vide", "vide", "vide"],["vide", "arbre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual(0, nbCellFeuVoisine(automate, 3, 3, 1, 1))
        automate = [["vide", "feu", "vide"],["vide", "arbre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual(1, nbCellFeuVoisine(automate, 3, 3, 1, 1))
        automate = [["vide", "feu", "vide"],["feu", "arbre", "vide"],["vide", "vide", "vide"]]
        self.assertEqual(2, nbCellFeuVoisine(automate, 3, 3, 1, 1))
        automate = [["vide", "feu", "vide"],["feu", "arbre", "feu"],["vide", "vide", "vide"]]
        self.assertEqual(3, nbCellFeuVoisine(automate, 3, 3, 1, 1))
        automate = [["vide", "feu", "vide"],["feu", "arbre", "feu"],["vide", "feu", "vide"]]
        self.assertEqual(4, nbCellFeuVoisine(automate, 3, 3, 1, 1))

